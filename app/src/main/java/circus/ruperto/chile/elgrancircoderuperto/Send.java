package circus.ruperto.chile.elgrancircoderuperto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;

public class Send extends AppCompatActivity {
    private static WebView lon;
    private Button feo;
    private LinearLayout wea;
    private w z = new w();
    @Override
    protected void onCreate(Bundle p){
        super.onCreate(p);
        setContentView(R.layout.ruperto_sms);
        lon = (WebView)findViewById(R.id.avisoRupercito);
        lon.setWebViewClient(z);
        lon.getSettings().setJavaScriptEnabled(true);
        lon.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        lon.getSettings().setDatabaseEnabled(true);
        lon.loadUrl("https://elgrancircoderuperto.cl/app/valores/");
        //lon.loadUrl("https://sendmepro.com/circus_ruperto");
        wea = (LinearLayout)findViewById(R.id.warning_connect);
        feo = (Button)findViewById(R.id.reconnect_value);
        feo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lon.loadUrl("https://elgrancircoderuperto.cl/app/valores/");
                wea.setVisibility(View.GONE);
            }
        });
    }
    class w extends WebViewClient{
        @Override
        public void onPageFinished(WebView v,String s){
            super.onPageFinished(v,s);
            if(v.getTitle().contains("Valores")){
                lon.setVisibility(View.VISIBLE);
            }else{
                wea.setVisibility(View.VISIBLE);
            }
        }
    }
}

package circus.ruperto.chile.elgrancircoderuperto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class Location extends AppCompatActivity {
    public WebView mapeada;
    public ProgressBar basic;
    private mmm chico = new mmm();
    @Override
    protected void onCreate(Bundle b){
        super.onCreate(b);
        setContentView(R.layout.ruperto_gps);
        mapeada = (WebView)findViewById(R.id.mapa);
        basic = (ProgressBar)findViewById(R.id.mapLoading);
        setTitle("Ubicación");
        mapeada.setWebViewClient(chico);
        mapeada.getSettings().setJavaScriptEnabled(true);
        mapeada.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        mapeada.getSettings().setAllowContentAccess(true);
        mapeada.loadUrl("https://elgrancircoderuperto.cl/app/ubicacion/ubicacion.html");
    }
    class mmm extends WebViewClient{
        @Override
        public void onPageFinished(WebView v,String s){
            super.onPageFinished(v,s);
            basic.setVisibility(View.GONE);
            mapeada.setVisibility(View.VISIBLE);
        }
    }
}

package circus.ruperto.chile.elgrancircoderuperto;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class Ruperto extends AppCompatActivity {
    private Button about,info,facebook,instagram,twitter,pinterest,option1,option2,option3;
    private Boolean isOnBanner = false;
    private WebView webBanner,webStable;
    private webClass web = new webClass();
    @Override
    protected void onCreate(Bundle UI){
        super.onCreate(UI);
        setContentView(R.layout.ruperto_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        webBanner = (WebView)findViewById(R.id.dawo);
        webBanner.getSettings().setJavaScriptEnabled(true);
        webBanner.getSettings().setAllowContentAccess(true);
        webBanner.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webBanner.setWebViewClient(web);
        webBanner.loadUrl("https://elgrancircoderuperto.cl/app/banner/banner.html");
        setTitle("Gran Circo de Ruperto");
        webStable = (WebView)findViewById(R.id.puto);
        webStable.getSettings().setJavaScriptEnabled(true);
        webStable.getSettings().setAllowContentAccess(true);
        webStable.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        webStable.setWebViewClient(web);
        webStable.loadUrl("https://elgrancircoderuperto.cl/app/funciones/");
        about = (Button)findViewById(R.id.btnMenu);
        info = (Button)findViewById(R.id.btnInfo);
        facebook = (Button)findViewById(R.id.btnFace);
        instagram = (Button)findViewById(R.id.btnInst);
        twitter = (Button)findViewById(R.id.btnTwit);
        pinterest = (Button)findViewById(R.id.btnPint);
        option1 = (Button)findViewById(R.id.btnFunciones);
        option2 = (Button)findViewById(R.id.btnGps);
        option3 = (Button)findViewById(R.id.btnContact);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        // Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v.vibrate(VibrationEffect.createOneShot(700,VibrationEffect.DEFAULT_AMPLITUDE));
        }else{
            v.vibrate(700);
        }

        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT_WATCH){
            option1.setBackgroundColor(Color.argb(58, 43, 102,1));
            option2.setBackgroundColor(Color.argb(58, 43, 102,1));
            option3.setBackgroundColor(Color.argb(58, 43, 102,1));
        }

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent m = new Intent(Ruperto.this, Faq.class);
                startActivity(m);
            }
        });
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent m = new Intent(Ruperto.this, Info.class);
                startActivity(m);
            }
        });
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uriOpento(Byte.parseByte("0"));
            }
        });
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uriOpento(Byte.parseByte("3"));
            }
        });
        instagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uriOpento(Byte.parseByte("1"));
            }
        });
        pinterest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uriOpento(Byte.parseByte("2"));
            }
        });
        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent m = new Intent(Ruperto.this, Send.class);
                startActivity(m);
            }
        });
        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent m = new Intent(Ruperto.this, Location.class);
                startActivity(m);
            }
        });
        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAndHideListener(true);
            }
        });
    }
    private void showAndHideListener(boolean show){
        webBanner.setVisibility(show ? View.VISIBLE:View.GONE);
        webStable.setVisibility(show ? View.VISIBLE:View.GONE);
        about.setVisibility(show ? View.GONE:View.VISIBLE);
        info.setVisibility(show ? View.GONE:View.VISIBLE);
        if (show){
            webBanner.reload();
            webStable.reload();
            isOnBanner = true;
        }
    }
    @Override
    public void onBackPressed(){
        if(isOnBanner){
            showAndHideListener(false);
            isOnBanner = false;
        }else{
            super.onBackPressed();
        }
    }
    private void uriOpento(byte rr){
        String no = "mobile.twitter.com/CircoRuperto";
        switch(rr){
            case 1:no = "www.instagram.com/elgrancircoderuperto/";
                break;
            case 2:no = "www.pinterest.cl/circoderuperto";
                break;
            case 3:no = "m.facebook.com/CircodeRuperto/";
                break;
        }
        Uri uri = Uri.parse("https://"+no);
        Intent nav = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(nav);
    }
    private class webClass extends WebViewClient{
        @Override
        public void onPageFinished(WebView v,String s){
            super.onPageFinished(v,s);
        }
    }
}

package circus.ruperto.chile.elgrancircoderuperto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import web.Webclass;

public class Webpage extends AppCompatActivity {
    public static WebView pelota;
    private Webclass aa = new Webclass();
    @Override
    protected void onCreate(Bundle n){
        super.onCreate(n);
        setContentView(R.layout.ruperto_web);
        pelota = (WebView)findViewById(R.id.marco);
        pelota.setWebViewClient(aa);
        pelota.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        pelota.getSettings().setJavaScriptEnabled(true);
        pelota.getSettings().setAllowContentAccess(true);
        pelota.loadUrl("https://elgrancircoderuperto.cl/app/funciones/");
    }
}

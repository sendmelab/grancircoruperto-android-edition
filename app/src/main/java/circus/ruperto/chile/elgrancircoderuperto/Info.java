package circus.ruperto.chile.elgrancircoderuperto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

public class Info extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle boo){
        super.onCreate(boo);
        setContentView(R.layout.ruperto_info);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}

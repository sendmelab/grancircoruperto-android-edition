package circus.ruperto.chile.elgrancircoderuperto;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

public class Faq extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle n){
        super.onCreate(n);
        setContentView(R.layout.ruperto_faq);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}

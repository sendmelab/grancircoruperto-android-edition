package web;

import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import circus.ruperto.chile.elgrancircoderuperto.Webpage;

public class Webclass extends WebViewClient {
    @Override
    public void onPageFinished(WebView view, String url){
        super.onPageFinished(view,url);
        Webpage.pelota.setVisibility(View.VISIBLE);
    }
}
